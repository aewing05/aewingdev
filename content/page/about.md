---
title: About me
comments: false
---

## About Me
I'm Andy, a software engineering manager with a background in development and analytics. I also like to do projects around the house, read books, ride my bike, and more.

## About This Blog
I'm running this on Hugo - a Go framework for static site generation. I chose Hugo over Jekyll and others because I wanted to become more familiar with Go. You don't need to know Go to create and populate a Hugo site, but I figured I can learn the language as I learn how Hugo actually builds the site from a series of markdown posts, templates, etc. 

I don't know where this blog is heading - I have a pretty eclectic set of interests, and I imagine a lot of them will show themselves here.
